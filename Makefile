all:
	pdflatex -output-directory=build FFR101.tex 
	pdflatex -output-directory=build FFR101.tex

clean:
	rm -rf build

compile:
	mkdir -p build
	make ref
	cd build; makeglossaries FFR101
	pdflatex -output-directory=build FFR101.tex 

ref:
	pdflatex -output-directory=build FFR101.tex
	bibtex build/FFR101.aux
	pdflatex -output-directory=build FFR101.tex
	pdflatex -output-directory=build FFR101.tex

